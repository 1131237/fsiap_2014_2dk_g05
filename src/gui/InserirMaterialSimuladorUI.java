/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import controller.SimuladorController;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.FileNotFoundException;
import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.ListCellRenderer;
import javax.swing.ListSelectionModel;
import javax.swing.border.EmptyBorder;
import model.Experiencia;
import model.Material;

/**
 *
 * @author Daniel
 */
public class InserirMaterialSimuladorUI{
    private final ResourceBundle idioma;
    private Experiencia exp;
    private final SimuladorController m_controllerIMS;
    private List<Material> lmat;
    
    public InserirMaterialSimuladorUI(ResourceBundle idioma, Experiencia experiencia, SimuladorController sctr) {
        this.idioma=idioma; 
        exp = experiencia;
        
        this.m_controllerIMS= sctr;
        int x;
            if(this.idioma.getString("lingua").equalsIgnoreCase("pt")) {
                x=0;}
            else{
                x=1;
            }
        lmat = m_controllerIMS.getListaMateriais(x);
        
        DialogInsMaterial diMaterial = new DialogInsMaterial(idioma);
        
        
    }
    
    private class DialogInsMaterial extends JDialog {
        CheckListItem[] checkList;
        private final ResourceBundle idioma;
        
        public DialogInsMaterial(ResourceBundle idioma) {
            this.idioma=idioma;
            JPanel p = criarPainelMateriais();
            JPanel p2 = criarPainelBotao(idioma);

            setLayout(new BorderLayout());
            JPanel painel = new JPanel(new BorderLayout());
            painel.add(p, BorderLayout.CENTER);
            add(painel, BorderLayout.CENTER);
            add(p2, BorderLayout.SOUTH);

            setResizable(false);
            setVisible(true);
            this.setLocation(480, 200);
            pack();
        }

        private JPanel criarPainelMateriais() {
            final int MARGEM_SUPERIOR = 10, MARGEM_INFERIOR = 0;
            final int MARGEM_ESQUERDA = 10, MARGEM_DIREITA = 0;

            JLabel lbl = new JLabel(this.idioma.getString("InsSimulador"), FlowLayout.RIGHT);
            lbl.setPreferredSize(lbl.getPreferredSize());

            this.checkList = toCheckListItem(lmat);
            JList list = new JList(checkList);

            list.setCellRenderer(new CheckListRenderer());
            list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
            list.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent event) {
                    JList list = (JList) event.getSource();
                    int index = list.locationToIndex(event.getPoint());
                    CheckListItem item = (CheckListItem) list.getModel().getElementAt(index);
                    item.setSelected(!item.isSelected());
                    list.repaint(list.getCellBounds(index, index));
                }
            });

            JPanel painel = new JPanel(new FlowLayout());
            painel.setBorder(new EmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA,
                    MARGEM_INFERIOR, MARGEM_DIREITA));

            painel.add(new JScrollPane(list));
            painel.add(lbl, FlowLayout.LEFT);

            return painel;
        }
        
        private JPanel criarPainelBotao(final ResourceBundle idioma) {
            JButton btnOk = new JButton("OK");
            btnOk.addActionListener(new ActionListener() {

                @Override
                public void actionPerformed(ActionEvent e) {

                    int materiaisSelecionados = contaMateriaisSelecionados();
                    if (materiaisSelecionados == 0) {
                        JOptionPane.showMessageDialog(null, 
                                idioma.getString("ExcNMaterial"),
                                idioma.getString("Erro"), JOptionPane.WARNING_MESSAGE, null);
                    } else {
                        for (int i = 0; i < checkList.length; i++) {
                            if (checkList[i].getSelected()) {
                                m_controllerIMS.addMaterialAbordado(checkList[i].getMaterial());
                            }
                        }
                       
                            ApresentarResultadoUI dialogAcresc = new ApresentarResultadoUI(idioma, exp, m_controllerIMS);
                        dispose();
                    }
                }
            });

            getRootPane().setDefaultButton(btnOk);

            JPanel p = new JPanel();
            final int MARGEM_SUPERIOR = 0, MARGEM_INFERIOR = 10;
            final int MARGEM_ESQUERDA = 10, MARGEM_DIREITA = 10;
            p.setBorder(new EmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA,
                    MARGEM_INFERIOR, MARGEM_DIREITA));
            p.add(btnOk);

            p.add(btnOk, JPanel.CENTER_ALIGNMENT);

            return p;
        }

        private int contaMateriaisSelecionados() {
            int numMateriaisSelecionados = 0;

            for (int i = 0; i < checkList.length; i++) {
                if (checkList[i].getSelected()) {
                    numMateriaisSelecionados++;
                }
            }

            return numMateriaisSelecionados;
        }
    }
    
    class CheckListItem {

        private final Material m_material;
        private boolean isSelected = false;

        public CheckListItem(Material material) {
            this.m_material= material;
        }

        public boolean isSelected() {
            return isSelected;
        }

        public void setSelected(boolean isSelected) {
            this.isSelected = isSelected;
        }

        public boolean getSelected() {
            return this.isSelected;
        }

        public Material getMaterial() {
            return this.m_material;
        }

        @Override
        public String toString() {
            return m_material.getNome();
        }
    }

    class CheckListRenderer extends JCheckBox implements ListCellRenderer {

        @Override
        public Component getListCellRendererComponent(JList list, Object value,
                int index, boolean isSelected, boolean hasFocus) {
            setEnabled(list.isEnabled());
            setSelected(((CheckListItem) value).isSelected());
            setFont(list.getFont());
            setBackground(list.getBackground());
            setForeground(list.getForeground());
            setText(value.toString());
            return this;
        }
    }

    private CheckListItem[] toCheckListItem(List<Material> material) {
        CheckListItem[] checkList = new CheckListItem[material.size()];

        for (int i = 0; i < checkList.length; i++) {
            Material m = material.get(i);
            if (m != null) {
                checkList[i] = new CheckListItem(m);
            }
        }

        return checkList;
    }
}
