/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import imp.ExportarHtml;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Experiencia;
import model.Simulacao;

/**
 *
 * @author Bruno
 */
public class ExportarHtmlController {
    private ExportarHtml export;
    private Experiencia exp;
    
    public ExportarHtmlController(){
        export = new ExportarHtml();
    }

    public void criarHtmlPT(Simulacao sim) throws FileNotFoundException, UnsupportedEncodingException{
       export.setSim(sim);
       export.settExterior(sim.gettExterior());
       export.settInterior(sim.gettInterior());
       export.setMatUtilizar(sim.getMatUtilizar());
       export.setLargura(sim.getLargura());
       export.setRes_simulacao(sim.getRes_Simulacao());
       export.setResistencia(sim.getResistencia());

        try {
            if(sim.getResistencia()<1.60){
            export.criarHtmlPTMau();
            }else{
                export.criarHtmlPTBom();
            }
        } catch (IOException ex) {
            Logger.getLogger(ExportarHtmlController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
        public void criarHtmlEN(Simulacao sim) throws FileNotFoundException, UnsupportedEncodingException{
       export.setSim(sim);
       export.settExterior(sim.gettExterior());
       export.settInterior(sim.gettInterior());
       export.setMatUtilizar(sim.getMatUtilizar());
       export.setLargura(sim.getLargura());
       export.setRes_simulacao(sim.getRes_Simulacao());
       export.setResistencia(sim.getResistencia());

        try {
            if(sim.getResistencia()<1.60){
            export.criarHtmlENMau();
            }else{
                export.criarHtmlENBom();
            }
        } catch (IOException ex) {
            Logger.getLogger(ExportarHtmlController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public Experiencia getExp() {
        return exp;
    }

    public void setExp(Experiencia exp) {
        this.exp = exp;
        
    }

    public int getX() {
        return this.export.getX();
    }
    
    


}
