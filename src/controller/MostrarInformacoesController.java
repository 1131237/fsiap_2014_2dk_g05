/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.util.List;
import model.Experiencia;
import model.ListaMateriais;
import model.Material;

/**
 *
 * @author 1130429
 */
public class MostrarInformacoesController {
    
    private Experiencia exp;
    
    public MostrarInformacoesController(Experiencia experiencia, String material){
        exp = experiencia;
    }
    
    public List<Material> getListaMateriais(int x) {
        if(x==0){
        return exp.getLm_pt().getListaMateriais();
        }else{
            System.out.println(x);
            return exp.getLm_en().getListaMateriais();
        }
    }
    
    public List<Double> getListaCondutibilidade() {
        ListaMateriais l =exp.getLm_pt();
        return l.getListaCondutibilidade();
     
      
    }
    
}
