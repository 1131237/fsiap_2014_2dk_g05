/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import model.Experiencia;
import model.Material;

/**
 *
 * @author 1130429 
 */
public class AcrescentarMaterialController {
    private Experiencia exp;
    
    public AcrescentarMaterialController(Experiencia experiencia){
        this.exp = experiencia;
    }

    public boolean addMaterial(String nome, double constante, int x) {
        Material m = new Material(nome, constante);
        if(x==0){
           return exp.addMaterialPT(m);
        }else{
           return exp.addMaterialEN(m);
    
        }
}}
