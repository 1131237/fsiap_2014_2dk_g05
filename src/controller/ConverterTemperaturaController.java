/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

/**
 *
 * @author Bruno
 */

import model.Conversões;

public class ConverterTemperaturaController {
    private Conversões conversao;
    
    public ConverterTemperaturaController(Conversões conversao){
        this.conversao=conversao;
    }
    
    public double KelvinToCelsius(double kelvin){
        
        double celsius = this.conversao.KelvinToCelsius(kelvin);
        
        return celsius;
    }

    public double CelsiusToKelvin(double celsius) {
        
        double kelvin = this.conversao.CelsiusToKelvin(celsius);
        
        return kelvin;
    }

    public double CelsiusToFahrenheit(double celsius) {
        
        double fahrenheit = this.conversao.CelsiusToFahrenheit(celsius);
        
        return fahrenheit;
        
    }

    public double FahrenheitToCelsius(double fahrenheit) {
        
        double celsius = this.conversao.FahrenheitToCelsius(fahrenheit);
        
        return celsius;
    }

    public double FahrenheitToKelvin(double fahrenheit) {
        
        double kelvin = this.conversao.FahrenheitToKelvin(fahrenheit);
        
        return kelvin;
    }

    public double KelvinToFahrenheit(double kelvin) {
        
        double fahrenheit = this.conversao.KelvinToFahrenheit(kelvin);
        
        return fahrenheit;
    }
}
