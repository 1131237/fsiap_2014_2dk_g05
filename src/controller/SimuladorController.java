/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.util.ArrayList;
import java.util.List;
import model.Experiencia;
import model.FormulaSimulacao;
import model.Material;
import model.Simulacao;

/**
 *
 * @author Daniel
 */
public class SimuladorController {
     private Experiencia exp;
     private Simulacao simulacao= new Simulacao();
    
    public SimuladorController(Experiencia experiencia){
        exp = experiencia;
        exp.setSim(simulacao);
    }
    
    public List<Material> getListaMateriais(int x) {
        if(x==0){
        return exp.getLm_pt().getListaMateriais();
        }else{
            return exp.getLm_en().getListaMateriais();
        }
    }
    
    public boolean addMaterialAbordado(Material m) {
        return this.simulacao.addMaterialAbordado(m);
    }
    
    public List<Material> getMateriais() {       
        return this.simulacao.getMatUtilizar();
    }

    public int gettExterior() {
        return this.simulacao.gettExterior();
    }

    public void settExterior(int tExterior) {
        this.simulacao.settExterior(tExterior);
    }

    public int gettInterior() {
        return this.simulacao.gettInterior();
    }

    public void settInterior(int tInterior) {
        this.simulacao.settInterior(tInterior);
    }

    public double getLargura() {
        return this.simulacao.getLargura();
    }

    public void setLargura(double largura) {
        this.simulacao.setLargura(largura);
    }
    
     public double calcular() {        
        return this.simulacao.calcular();
    }
     
      public double calcular_res() {        
        return this.simulacao.calcular_resistencia();
    }
}
