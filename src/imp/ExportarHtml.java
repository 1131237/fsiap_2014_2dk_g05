/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package imp;

import java.awt.Desktop;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Formatter;
import java.util.List;
import java.util.Scanner;
import model.Experiencia;
import model.Material;
import model.Simulacao;

/**
 *
 * @author Bruno
 */
public class ExportarHtml {
    
    private static final Scanner in = new Scanner(System.in);   
    static Formatter outc;
    private static int x;
    private Experiencia exp;
    private int tExterior;
    private int tInterior;
    private double largura;
    private double res_simulacao;
    private double resistencia;
    private Simulacao sim;
    private List<Material> matUtilizar= new ArrayList<>();
    
    public ExportarHtml(){
        x=0;
        
    }
    
        public void criarHtmlPTMau() throws FileNotFoundException, UnsupportedEncodingException, IOException{
        
        
        String nome = "Resultado_"+x+".html";
        String materiais = materiais();
            
            outc = new Formatter(new File(nome),"UTF-8"); 
            outc.format("<!doctype html>\n" +
"<html>\n" +
"<head>\n" +
"<meta charset=\"utf-8\">\n" +
"<title>Modelo</title>\n" +
"<style type=\"text/css\">\n" +
"h1 {\n" +
"	font-size: 36px;\n" +
"	color: #FFF3F3;\n" +
"	text-shadow: 2px 2px 2px #000000;\n" +
"	opacity: 1;\n" +
"	background-origin: padding-box;\n" +
"	background-clip: padding-box;\n" +
"	visibility: visible;\n" +
"	display: inherit;\n" +
"	/* [disabled]border-color: #C0B9B9; */\n" +
"	text-align: center;\n" +
"}\n" +
"h2 {\n" +
"	font-size: 24px;\n" +
"	color: #FFF3F3;\n" +
"	text-shadow: 2px 2px 2px #000000;\n" +
"	text-align: center;\n" +
"}\n" +
"body {\n" +
"	background-image: url(Fundo.jpg);\n" +
"	background-repeat: no-repeat;\n" +
"	background-attachment: fixed;\n" +
"	background-size:cover;\n" +
"}\n" +
"h3 {\n" +
"	font-size: 16px;\n" +
"	color: #FFFFFF;\n" +
"}\n" +
"</style>\n" +
"</head>\n" +
"\n" +
"<body>\n" +
"<div style = \"text-align:center;\">\n" +
"  <h1>SIMULAÇÃO</h1>\n" +
"</div>\n" +
"<div style = \"text-align:center;\">\n" +
"  <pre style=\"color: #FFF3F3\">Nesta página, ser-lhe-ão apresentados os resultados da simulação efectuada. Esta página serve, para lhe mostrar o comportamento dos\n" +
" materiais isoladores que escolheu e dizer-lhe se são, ou não, uma boa escolha para o isolamento da sua casa.</pre>\n" +
"</div>\n" +
"<pre style=\"color: #939393\"><br></pre>\n" +
"<h2 style = \"text-align:center;\">MATERIAIS E FACTORES</h2>\n" +
"<div style = \"text-align:center;\">\n" +
"  <pre style=\"color: #FFF3F3; text-align:center;\">"+materiais +
"</pre>\n" +
"  <pre style=\"color: #FFF3F3; text-align:center;\">Espessura - " + largura/100 + " m"+
"</pre>\n" +
"</div>\n" +
"<h2 style = \"text-align:center;\">&nbsp;</h2>\n" +
"<h2 style = \"text-align:center;\">RESULTADO</h2>\n" +
"<div style = \"text-align:center;\"></div>\n" +
"<div style=\"text-align:center;\">\n" +
"<pre style = \"background-image: url(Green-Living-Green-Home.jpg); background-size: contain; color: #FFFFFF; background-attachment: scroll; background-repeat: no-repeat; text-align: center; width: 754px; height: 459px; background-position:center;display:block;margin-left: auto; margin-right: auto\">\n" +
"\n" +
"\n" +
"\n" +
"\n" +
"\n" +
"\n" +
"\n" +
"\n" +
"\n" +
"\n" +
"\n" +
"\n" +
"\n" +
"\n" +
"Exterior: "+tExterior+"                                 Interior: "+tInterior+"</pre>\n" +
"                 </div>\n" +
"<h3 style =\"text-align:center;\">Fluxo de Calor - "+res_simulacao+" W"+
"<h3 style =\"text-align:center;\">Resistência Térmica - "+resistencia+" m<sup>2</sup>K/W"+"</h3>\n" +                    
"<h2>Conclusão&nbsp;</h2>\n" +
"<p>&nbsp;</p>\n" +
"<div style = \"text-align:center;\">\n" +
"  <pre style=\"color: #FFF3F3\">Podemos concluir, através do resultado desta simulação, que o isolamento escolhido não é o mais apropriado. Caso haja temperaturas\n" +
"muito acima ou muito abaixo do desejado, a temperatura interior da sua casa irá variar muito em função desses valores. Tente\n" +
"utilizar materiais com melhor capacidade termica para que o isolamento escolhido obdeça aos minimos fixados pelo RCCTE(Regulamento das Características de\n"
                    + " Comportamento Térmico dos Edifícios).  </pre>\n" +
"  <pre style=\"color: #FFF3F3; text-align:center;\">&nbsp;</pre>\n" +
"</div>\n" +
"</body>\n" +
"</html>");
        
        outc.close();
        File htmlFile = new File(nome);
        Desktop.getDesktop().browse(htmlFile.toURI());
        x++;
        }
        
        public void criarHtmlPTBom() throws FileNotFoundException, UnsupportedEncodingException, IOException{
        
        
        String nome = "Resultado_"+x+".html";
        String materiais = materiais();
            
            outc = new Formatter(new File(nome),"UTF-8"); 
            outc.format("<!doctype html>\n" +
"<html>\n" +
"<head>\n" +
"<meta charset=\"utf-8\">\n" +
"<title>Modelo</title>\n" +
"<style type=\"text/css\">\n" +
"h1 {\n" +
"	font-size: 36px;\n" +
"	color: #FFF3F3;\n" +
"	text-shadow: 2px 2px 2px #000000;\n" +
"	opacity: 1;\n" +
"	background-origin: padding-box;\n" +
"	background-clip: padding-box;\n" +
"	visibility: visible;\n" +
"	display: inherit;\n" +
"	/* [disabled]border-color: #C0B9B9; */\n" +
"	text-align: center;\n" +
"}\n" +
"h2 {\n" +
"	font-size: 24px;\n" +
"	color: #FFF3F3;\n" +
"	text-shadow: 2px 2px 2px #000000;\n" +
"	text-align: center;\n" +
"}\n" +
"body {\n" +
"	background-image: url(Fundo.jpg);\n" +
"	background-repeat: no-repeat;\n" +
"	background-attachment: fixed;\n" +
"	background-size:cover;\n" +
"}\n" +
"h3 {\n" +
"	font-size: 16px;\n" +
"	color: #FFFFFF;\n" +
"}\n" +
"</style>\n" +
"</head>\n" +
"\n" +
"<body>\n" +
"<div style = \"text-align:center;\">\n" +
"  <h1>SIMULAÇÃO</h1>\n" +
"</div>\n" +
"<div style = \"text-align:center;\">\n" +
"  <pre style=\"color: #FFF3F3\">Nesta página, ser-lhe-ão apresentados os resultados da simulação efectuada. Esta página serve, para lhe mostrar o comportamento dos\n" +
" materiais isoladores que escolheu e dizer-lhe se são, ou não, uma boa escolha para o isolamento da sua casa.</pre>\n" +
"</div>\n" +
"<pre style=\"color: #939393\"><br></pre>\n" +
"<h2 style = \"text-align:center;\">MATERIAIS E FACTORES</h2>\n" +
"<div style = \"text-align:center;\">\n" +
"  <pre style=\"color: #FFF3F3; text-align:center;\">"+materiais +
"</pre>\n" +
"  <pre style=\"color: #FFF3F3; text-align:center;\">Espessura - " + largura/100 + " m"+
"</pre>\n" +
"</div>\n" +
"<h2 style = \"text-align:center;\">&nbsp;</h2>\n" +
"<h2 style = \"text-align:center;\">RESULTADO</h2>\n" +
"<div style = \"text-align:center;\"></div>\n" +
"<div style=\"text-align:center;\">\n" +
"<pre style = \"background-image: url(Green-Living-Green-Home.jpg); background-size: contain; color: #FFFFFF; background-attachment: scroll; background-repeat: no-repeat; text-align: center; width: 754px; height: 459px; background-position:center;display:block;margin-left: auto; margin-right: auto\">\n" +
"\n" +
"\n" +
"\n" +
"\n" +
"\n" +
"\n" +
"\n" +
"\n" +
"\n" +
"\n" +
"\n" +
"\n" +
"\n" +
"\n" +
"Exterior: "+tExterior+"                                 Interior: "+tInterior+"</pre>\n" +
"                 </div>\n" +
"<h3 style =\"text-align:center;\">Fluxo de Calor - "+res_simulacao+" W"+
"<h3 style =\"text-align:center;\">Resistência Térmica - "+resistencia+" m<sup>2</sup>K/W"+"</h3>\n" +                    
"<h2>Conclusão&nbsp;</h2>\n" +
"<p>&nbsp;</p>\n" +
"<div style = \"text-align:center;\">\n" +
"  <pre style=\"color: #FFF3F3\">Podemos concluir, através do resultado desta simulação, que o isolamento escolhido é o apropriado. Caso haja temperaturas\n" +
"muito acima ou muito abaixo do desejado, a temperatura interior da sua casa irá manter-se perto dos valores desejados, não oscilando muito. \n" +
"O seu isolamento, cumpre assim o estabelecido no RCCTE - Regulamento das Características de Comportamento Térmico dos Edifícios\n"
                    + " Comportamento Térmico dos Edifícios).  </pre>\n" +
"  <pre style=\"color: #FFF3F3; text-align:center;\">&nbsp;</pre>\n" +
"</div>\n" +
"</body>\n" +
"</html>");
        
        outc.close();
        File htmlFile = new File(nome);
        Desktop.getDesktop().browse(htmlFile.toURI());
        x++;
        }
        
        public String materiais(){
            
            String mat = "";
            
            for(Material m : this.matUtilizar){
                mat = mat+"- "+m.getNome()+"\n";
            }
            
            return mat;
        }

    public Experiencia getExp() {
        return exp;
    }

    public void setExp(Experiencia exp) {
        this.exp = exp;
    }

    public int gettExterior() {
        return tExterior;
    }

    public void settExterior(int tExterior) {
        this.tExterior = tExterior;
    }

    public int gettInterior() {
        return tInterior;
    }

    public void settInterior(int tInterior) {
        this.tInterior = tInterior;
    }

    public double getLargura() {
        return largura;
    }

    public void setLargura(double largura) {
        this.largura = largura;
    }

    public double getRes_simulacao() {
        return res_simulacao;
    }

    public void setRes_simulacao(double res_simulacao) {
        this.res_simulacao = Math.round((res_simulacao)*100.0)/100.0;
    }

    public Simulacao getSim() {
        return sim;
    }

    public void setSim(Simulacao sim) {
        this.sim = sim;
    }

    public List<Material> getMatUtilizar() {
        return matUtilizar;
    }

    public void setMatUtilizar(List<Material> matUtilizar) {
        this.matUtilizar = matUtilizar;
    }
        
        public void criarHtmlENMau() throws FileNotFoundException, UnsupportedEncodingException, IOException{
        
        
        String nome = "Result_"+x+".html";
        String materiais = materiais();
            
            outc = new Formatter(new File(nome),"UTF-8"); 
            outc.format("<!doctype html>\n" +
"<html>\n" +
"<head>\n" +
"<meta charset=\"utf-8\">\n" +
"<title>Modelo</title>\n" +
"<style type=\"text/css\">\n" +
"h1 {\n" +
"	font-size: 36px;\n" +
"	color: #FFF3F3;\n" +
"	text-shadow: 2px 2px 2px #000000;\n" +
"	opacity: 1;\n" +
"	background-origin: padding-box;\n" +
"	background-clip: padding-box;\n" +
"	visibility: visible;\n" +
"	display: inherit;\n" +
"	/* [disabled]border-color: #C0B9B9; */\n" +
"	text-align: center;\n" +
"}\n" +
"h2 {\n" +
"	font-size: 24px;\n" +
"	color: #FFF3F3;\n" +
"	text-shadow: 2px 2px 2px #000000;\n" +
"	text-align: center;\n" +
"}\n" +
"body {\n" +
"	background-image: url(Fundo.jpg);\n" +
"	background-repeat: no-repeat;\n" +
"	background-attachment: fixed;\n" +
"	background-size:cover;\n" +
"}\n" +
"h3 {\n" +
"	font-size: 16px;\n" +
"	color: #FFFFFF;\n" +
"}\n" +
"</style>\n" +
"</head>\n" +
"\n" +
"<body>\n" +
"<div style = \"text-align:center;\">\n" +
"  <h1>SIMULATION</h1>\n" +
"</div>\n" +
"<div style = \"text-align:center;\">\n" +
"  <pre style=\"color: #FFF3F3\">On this page, you will be presented with the results of the performed simulation. This page show you the behavior of\n" +
" the insluators materials that you have selected and tell you whether they are or not a good choice for the isulation of your home.</pre>\n" +
"</div>\n" +
"<pre style=\"color: #939393\"><br></pre>\n" +
"<h2 style = \"text-align:center;\">MATERIALS AND FACTORS</h2>\n" +
"<div style = \"text-align:center;\">\n" +
"  <pre style=\"color: #FFF3F3; text-align:center;\">"+materiais +
"</pre>\n" +
"<pre style=\"color: #FFF3F3; text-align:center;\">Thickness - " + largura/100 + " m"+
"</pre>\n" +
"</div>\n" +
"<h2 style = \"text-align:center;\">&nbsp;</h2>\n" +
"<h2 style = \"text-align:center;\">RESULT</h2>\n" +
"<div style = \"text-align:center;\"></div>\n" +
"<div style=\"text-align:center;\">\n" +
"<pre style = \"background-image: url(Green-Living-Green-Home.jpg); background-size: contain; color: #FFFFFF; background-attachment: scroll; background-repeat: no-repeat; text-align: center; width: 754px; height: 459px; background-position:center;display:block;margin-left: auto; margin-right: auto\">\n" +
"\n" +
"\n" +
"\n" +
"\n" +
"\n" +
"\n" +
"\n" +
"\n" +
"\n" +
"\n" +
"\n" +
"\n" +
"\n" +
"\n" +
"Outside: "+tExterior+"                                 Inside: "+tInterior+"</pre>\n" +
"                 </div>\n" +
"<h3 style=\"text-align:center;\">Heat Flux - "+res_simulacao+" W</h3>\n" +
"<h3 style=\"text-align:center;\"Thermal Resistance - "+resistencia+" m<sup>2</sup>K/W"+"</h3>\n" +
"<h2>CONCLUSION&nbsp;</h2>\n" +
"<p>&nbsp;</p>\n" +
"<div style = \"text-align:center;\">\n" +
"  <pre style=\"color: #FFF3F3\">We can conclude by the results of this simulation that the chosen isolation is not the most appropriate one. If there are \n" +
"temperatures well above or well below the desired interior temperature of your home will vary greatly depending on these values. Try\n" +
"using materials with better thermal capacity so it fits in the RCCTE(portuguese sigles for the Regulation of Caractheristics\n "
+ "of the Thermal Behaviour of Buildings).  </pre>\n" +
"  <pre style=\"color: #FFF3F3; text-align:center;\">&nbsp;</pre>\n" +
"</div>\n" +
"</body>\n" +
"</html>");
        
        outc.close();
        File htmlFile = new File(nome);
        Desktop.getDesktop().browse(htmlFile.toURI());
        x++;
        }

         public void criarHtmlENBom() throws FileNotFoundException, UnsupportedEncodingException, IOException{
        
        
        String nome = "Result_"+x+".html";
        String materiais = materiais();
            
            outc = new Formatter(new File(nome),"UTF-8"); 
            outc.format("<!doctype html>\n" +
"<html>\n" +
"<head>\n" +
"<meta charset=\"utf-8\">\n" +
"<title>Modelo</title>\n" +
"<style type=\"text/css\">\n" +
"h1 {\n" +
"	font-size: 36px;\n" +
"	color: #FFF3F3;\n" +
"	text-shadow: 2px 2px 2px #000000;\n" +
"	opacity: 1;\n" +
"	background-origin: padding-box;\n" +
"	background-clip: padding-box;\n" +
"	visibility: visible;\n" +
"	display: inherit;\n" +
"	/* [disabled]border-color: #C0B9B9; */\n" +
"	text-align: center;\n" +
"}\n" +
"h2 {\n" +
"	font-size: 24px;\n" +
"	color: #FFF3F3;\n" +
"	text-shadow: 2px 2px 2px #000000;\n" +
"	text-align: center;\n" +
"}\n" +
"body {\n" +
"	background-image: url(Fundo.jpg);\n" +
"	background-repeat: no-repeat;\n" +
"	background-attachment: fixed;\n" +
"	background-size:cover;\n" +
"}\n" +
"h3 {\n" +
"	font-size: 16px;\n" +
"	color: #FFFFFF;\n" +
"}\n" +
"</style>\n" +
"</head>\n" +
"\n" +
"<body>\n" +
"<div style = \"text-align:center;\">\n" +
"  <h1>SIMULATION</h1>\n" +
"</div>\n" +
"<div style = \"text-align:center;\">\n" +
"  <pre style=\"color: #FFF3F3\">On this page, you will be presented with the results of the performed simulation. This page show you the behavior of\n" +
" the insluators materials that you have selected and tell you whether they are or not a good choice for the isulation of your home.</pre>\n" +
"</div>\n" +
"<pre style=\"color: #939393\"><br></pre>\n" +
"<h2 style = \"text-align:center;\">MATERIALS AND FACTORS</h2>\n" +
"<div style = \"text-align:center;\">\n" +
"  <pre style=\"color: #FFF3F3; text-align:center;\">"+materiais +
"</pre>\n" +
"<pre style=\"color: #FFF3F3; text-align:center;\">Thickness - " + largura/100 + " m"+
"</pre>\n" +
"</div>\n" +
"<h2 style = \"text-align:center;\">&nbsp;</h2>\n" +
"<h2 style = \"text-align:center;\">RESULT</h2>\n" +
"<div style = \"text-align:center;\"></div>\n" +
"<div style=\"text-align:center;\">\n" +
"<pre style = \"background-image: url(Green-Living-Green-Home.jpg); background-size: contain; color: #FFFFFF; background-attachment: scroll; background-repeat: no-repeat; text-align: center; width: 754px; height: 459px; background-position:center;display:block;margin-left: auto; margin-right: auto\">\n" +
"\n" +
"\n" +
"\n" +
"\n" +
"\n" +
"\n" +
"\n" +
"\n" +
"\n" +
"\n" +
"\n" +
"\n" +
"\n" +
"\n" +
"Outside: "+tExterior+"                                 Inside: "+tInterior+"</pre>\n" +
"                 </div>\n" +
"<h3 style=\"text-align:center;\">Heat Flux - "+res_simulacao+" W</h3>\n" +
"<h3 style=\"text-align:center;\"Thermal Resistance - "+resistencia+" m<sup>2</sup>K/W"+"</h3>\n" +
"<h2>CONCLUSION&nbsp;</h2>\n" +
"<p>&nbsp;</p>\n" +
"<div style = \"text-align:center;\">\n" +
"  <pre style=\"color: #FFF3F3\">We can conclude by the results of this simulation that the chosen isolation is the most appropriate one. If there are \n" +
"temperatures well above or well below the desired, the interior temperature of your home will stay stable for longer. Your\n" +
" fits in the RCCTE(portuguese sigles for the Regulation of Caractheristics\n "
+ "of the Thermal Behaviour of Buildings).  </pre>\n" +
"  <pre style=\"color: #FFF3F3; text-align:center;\">&nbsp;</pre>\n" +
"</div>\n" +
"</body>\n" +
"</html>");
        
        outc.close();
        File htmlFile = new File(nome);
        Desktop.getDesktop().browse(htmlFile.toURI());
        x++;
        }
        
    public int getX() {
        
        return x;
    }
    
    public void setX(int aux){
        
        this.x = aux;
    }

    public double getResistencia() {
        return resistencia;
    }

    public void setResistencia(double resistencia) {
        this.resistencia = Math.round((resistencia)*100.0)/100.0;
    }
    
    
        
}
