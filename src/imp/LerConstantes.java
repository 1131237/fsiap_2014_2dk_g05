/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package imp;

/**
 *
 * @author Bruno
 */
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import model.Material;
import model.ListaMateriais;
 
public class LerConstantes {
 
    public LerConstantes(){
        
        
        
    }

  public void importar_pt(ListaMateriais lm) {
 
	String csvFile = "src\\Resources\\Constantes.csv";
	BufferedReader br = null;
	String line = "";
	String cvsSplitBy = ";";

	try {
 
		br = new BufferedReader(new FileReader(csvFile));
		line = br.readLine();
                while ((line = br.readLine()) != null) {
 
		        // use comma as separator
			String[] constantes = line.split(cvsSplitBy);
                        String nome = constantes[0];
                        double cons = Double.parseDouble(constantes[1]);
 
                        Material aux = new Material(nome,cons);
                        lm.addMaterial(aux);
                        
		}
 
	} catch (FileNotFoundException e) {
		e.printStackTrace();
	} catch (IOException e) {
		e.printStackTrace();
	} finally {
		if (br != null) {
			try {
				br.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
 
	System.out.println("Done");
  }
  
  public void importar_en(ListaMateriais lm) {
 
	String csvFile = "src\\Resources\\Constantes_en.csv";
	BufferedReader br = null;
	String line = "";
	String cvsSplitBy = ";";

	try {
 
		br = new BufferedReader(new FileReader(csvFile));
		line = br.readLine();
                while ((line = br.readLine()) != null) {
 
		        // use comma as separator
			String[] constantes = line.split(cvsSplitBy);
                        String nome = constantes[0];
                        double cons = Double.parseDouble(constantes[1]);
 
                        Material aux = new Material(nome,cons);
                        lm.addMaterial(aux);
                        
		}
 
	} catch (FileNotFoundException e) {
		e.printStackTrace();
	} catch (IOException e) {
		e.printStackTrace();
	} finally {
		if (br != null) {
			try {
				br.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
 
	System.out.println("Done");
  }
 
}
