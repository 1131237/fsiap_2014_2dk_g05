/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;

/**
 *
 * @author 1130429
 */
public class Material implements Serializable {

    private String nome;
    private double constante;
    
    public Material() {
        this.nome = "";
        this.constante=0;
    }
    
    public Material(String nome, double constante){
        this.nome = nome;
        this.constante = constante;
    }
    
    public Material(Material material) {
       this.nome = material.getNome();
       this.constante = material.getConstante();
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public double getConstante() {
        return constante;
    }

    public void setConstante(double constante) {
        this.constante = constante;
    }

    
    
}
   
   
