/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.List;

/**
 *
 * @author Daniel
 */
public class FormulaSimulacao {
    public double calcularCondutividade(int tempInterior, int tempExterior, double largura, List<Material> materiais){

    largura=largura/100;
    double total=0;
    double condutividade=0;
    if(materiais.size()==1){    
        
        for(Material m : materiais){
             condutividade=m.getConstante(); 
        }
    
    double tempVariacao;
    if(tempExterior>tempInterior){
        tempVariacao=(tempExterior-tempInterior);
    }else{
        tempVariacao=(tempInterior-tempExterior);
    }
    
    total=condutividade*1*(tempVariacao/largura);
        
    return total;
      
    }else{
        double espessura_m=largura/materiais.size();
        for(Material m : materiais){
             condutividade= condutividade + espessura_m/m.getConstante(); 
        }
        double tempVariacao;
    if(tempExterior>tempInterior){
        tempVariacao=(tempExterior-tempInterior);
    }else{
        tempVariacao=(tempInterior-tempExterior);
    }
        total=1*tempVariacao/condutividade;
        return total;
    }
}
    
    public double calcularResistência(int tempInterior, int tempExterior, double largura, List<Material> materiais){
        
        largura = largura/100;
        
        double resistencia = 0;
        double aux = 0;
            if(materiais.size()==1){    
        
        for(Material m : materiais){
             aux=m.getConstante(); 
        }
    
        resistencia = largura/(aux*1);
        
    return resistencia;
    }else{
            double espessura_m=largura/materiais.size();
        for(Material m : materiais){
             resistencia = resistencia + (espessura_m/(m.getConstante()*1)); 
        }
        
        return resistencia;
    }     
                
  }
}
