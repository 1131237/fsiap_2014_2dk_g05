/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 *
 * @author 1130429
 */

public class ListaMateriais implements Serializable{
    private List<Material> listaMateriais;

    public ListaMateriais() {
        listaMateriais = new ArrayList<>();
    }

    public ListaMateriais(ListaMateriais listaMateriais) {
        this.listaMateriais = listaMateriais.getListaMateriais();
    }

    /**
     * Devolve uma instância de material.
     *
     * @return novo material
     */
    public Material novoMaterial() {
        return new Material();
    }

    /**
     * Adiciona a um material á lista de materiais.
     *
     * @param material material a adicionar
     * @return 
     */
    public boolean addMaterial(Material material) {
       
        for(Material m : listaMateriais){
            if(m.getNome().equalsIgnoreCase(material.getNome())){
                return false;
            }
        }
        this.listaMateriais.add(material);
        return true;
    }

    /**
     * Devolve a lista de materiais
     *
     * @return lista de materiais
     */
    public List<Material> getListaMateriais() {
        return listaMateriais;
    }
   
    /**
     * Este método clona uma lista de materiais. Devolve a lista de materiais
     * recebida mas noutra posição de memória e com os seus objetos também
     * noutra posição de memória.
     *
     * @param listaMateriais lista de materiais a clonar
     * @return lista de materiais
     */
    private List<Material> clonaListaMateriais(List<Material> listaMateriais) {
        List<Material> lm = new ArrayList<>();

        for (Material material : listaMateriais) {
            if (material != null) {
                lm.add(new Material(material));
            }
        }

        return lm;
    }

    @Override
    public String toString() {
        
        String st="";
        
        Iterator<Material> it = listaMateriais.iterator();
            
        for (Material aux : listaMateriais) {
		String nome  = aux.getNome();
                double cons = aux.getConstante();
                st+="Nome: "+nome+ "- Constante: "+cons+"\n";
	}
    
        return st;
    }
    
    
    public List<Double> getListaCondutibilidade() {
        List<Double> lc = new ArrayList<>();

        for (Material material : listaMateriais) {
           lc.add(material.getConstante());
        }

        return lc;
    }

    void setListaMateriais(List<Material> mat) {
           this.listaMateriais = mat;
    }
    
}