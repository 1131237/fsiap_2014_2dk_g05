/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author Bruno
 */
public class Conversões {
    
    
    public double CelsiusToFahrenheit(double celsius){
    
        double fahrenheit;
        
        fahrenheit =((9.0/5.0) *celsius + 32);
        double aux=Math.round((fahrenheit)*100.0)/100.0;
        
        return aux;
    
    
    }
    
    public double FahrenheitToCelsius (double fahrenheit){
        
        double celsius;
        
        celsius = Math.round(((5.0/9.0)*(fahrenheit - 32))*100.0)/100.0;
        
        return celsius;
    }
    
    public double CelsiusToKelvin (double celsius){
        
        double kelvin;
        
        kelvin = Math.round((celsius + 273.15)*100.0)/100.0;
        
        return kelvin;
        
    }
    
    public double KelvinToCelsius (double kelvin){
        
        double celsius;
        
        celsius = Math.round((kelvin - 273.15)*100.0)/100.0;
        
        return celsius;
    }
    
    public double KelvinToFahrenheit (double kelvin){
        
        double fahrenheit;
        
        fahrenheit = ((9.0/5.0)*(kelvin-273.15) + 32);
    
        
        return fahrenheit;
    }
    
    public double FahrenheitToKelvin (double farhenheit){
        
        double kelvin;
        
        kelvin = ((5.0/9.0)*(farhenheit - 32)+ 273.15);
        
        return kelvin;
    }
    
    
    
}
