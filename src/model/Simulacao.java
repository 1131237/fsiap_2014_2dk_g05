/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Daniel
 */
public class Simulacao {
    private List<Material> matUtilizar= new ArrayList<>();
     int tExterior;
     int tInterior;
     double largura;
     private FormulaSimulacao calculadora = new FormulaSimulacao();
     private double res_Simulacao;
     private double resistencia;

    public List<Material> getMatUtilizar() {
        return matUtilizar;
    }

    public void setMatUtilizar(List<Material> matUtilizar) {
        this.matUtilizar = matUtilizar;
    }

    public int gettExterior() {
        return tExterior;
    }

    public void settExterior(int tExterior) {
        this.tExterior = tExterior;
    }

    public int gettInterior() {
        return tInterior;
    }

    public void settInterior(int tInterior) {
        this.tInterior = tInterior;
    }

    public double getLargura() {
        return largura;
    }

    public void setLargura(double largura) {
        this.largura = largura;
    }
     
     public boolean addMaterialAbordado(Material m) {
        return this.matUtilizar.add(m);
    }
     
     public double calcular() {
        this.res_Simulacao=this.calculadora.calcularCondutividade(tInterior, tExterior, largura, matUtilizar);
        return this.res_Simulacao;
    }

     public double calcular_resistencia() {
        this.resistencia=this.calculadora.calcularResistência(tInterior, tExterior, largura, matUtilizar);
        return this.resistencia;
    }
     
    public double getRes_Simulacao() {
        return this.res_Simulacao;
    }     

    public double getResistencia() {
        return resistencia;
    }

    public void setResistencia(double resistencia) {
        this.resistencia = resistencia;
    }
    
    
     
}
