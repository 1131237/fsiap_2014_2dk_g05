/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package model;

import controller.ExportarHtmlController;
import imp.LerConstantes;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.List;

/**
 *
 * @author i130429
 */
public class Experiencia {
   
    private ListaMateriais lm_pt;
    private ListaMateriais lm_en;
    private LerConstantes lc;
    private Simulacao sim;
    private ExportarHtmlController exp;
    private final File file = new File("save.bin");
    
    public Experiencia() throws IOException, FileNotFoundException, ClassNotFoundException {
        lc = new LerConstantes();
        lm_pt = new ListaMateriais();
        lm_en = new ListaMateriais();
        exp= new ExportarHtmlController();
        
        if(!file.exists() || file.isDirectory()){
        
            file.createNewFile();
            lc.importar_en(lm_en);
            lc.importar_pt(lm_pt);
            
        }else if(file.length() == 0){
            lc.importar_en(lm_en);
            lc.importar_pt(lm_pt);
        }else{
            lerFicheiroBin();
        }
        
    }

    public ListaMateriais getLm_pt() {
        return lm_pt;
    }
    
    public ListaMateriais getLm_en() {
        return lm_en;
    }

    public void setLm(ListaMateriais lm) {
        this.lm_pt = lm;
    }

    public LerConstantes getLc() {
        return lc;
    }

    public void setLc(LerConstantes lc) {
        this.lc = lc;
    }

    public Simulacao getSim() {
        return sim;
    }

    public void setSim(Simulacao sim) {
        this.sim = sim;
    }

    public ExportarHtmlController getExp() {
        return exp;
    }

    public void setExp(ExportarHtmlController exp) {
        this.exp = exp;
    }

    public boolean addMaterialPT(Material m) {
        return this.lm_pt.addMaterial(m);
    }

    public boolean addMaterialEN(Material m) {
        return this.lm_en.addMaterial(m);
    }
    
    public void guardarFicheiroBin() throws IOException{
            ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(file));
            out.writeInt(exp.getX());
            out.writeObject(lm_pt.getListaMateriais());
            out.writeObject(lm_en.getListaMateriais());

        
    }

    private void lerFicheiroBin() throws FileNotFoundException, IOException, ClassNotFoundException{
            
            ObjectInputStream in = new ObjectInputStream(new FileInputStream(file));
            int x  = in.readInt();
            List<Material> mat_pt = (List<Material>) in.readObject();
            lm_pt.setListaMateriais(mat_pt);
            List<Material> mat_en = (List<Material>) in.readObject();
            lm_en.setListaMateriais(mat_en);
            
    }
    
}
