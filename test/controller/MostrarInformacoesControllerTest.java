/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;
import model.Experiencia;
import model.Material;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author ASUS
 */
public class MostrarInformacoesControllerTest {
    
    public MostrarInformacoesControllerTest() {
    }
    
  
    /**
     * Test of getListaMateriais method, of class MostrarInformacoesController.
     */
    @Test
    public void testGetListaMateriais() throws IOException, FileNotFoundException, ClassNotFoundException {
        System.out.println("getListaMateriais");
        Experiencia ex =new Experiencia();
        String m = "a";
        MostrarInformacoesController instance = new MostrarInformacoesController(ex, m);
        List<Material> expResult = instance.getListaMateriais(0);
        List<Material> result = instance.getListaMateriais(0);
        assertEquals(expResult, result);
    }

    /**
     * Test of getListaCondutibilidade method, of class MostrarInformacoesController.
     */
    @Test
    public void testGetListaCondutibilidade() throws IOException, FileNotFoundException, ClassNotFoundException {
        System.out.println("getListaCondutibilidade");
        Experiencia ex =new Experiencia();
        String m = "a";
        MostrarInformacoesController instance = new MostrarInformacoesController(ex, m);
        List<Double> expResult = instance.getListaCondutibilidade();
        List<Double> result = instance.getListaCondutibilidade();
        assertEquals(expResult, result);
       
    }
    
}
