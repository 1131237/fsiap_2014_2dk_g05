/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import model.Experiencia;
import model.Material;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Daniel
 */
public class SimuladorControllerTest {
    
    public SimuladorControllerTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of addMaterialAbordado method, of class SimuladorController.
     */
    @Test
    public void testAddMaterialAbordado() throws IOException, FileNotFoundException, ClassNotFoundException {
        System.out.println("addMaterialAbordado");
        Material m = new Material();
        SimuladorController instance = new SimuladorController (new Experiencia());
        boolean expResult = true;
        boolean result = instance.addMaterialAbordado(m);
        assertEquals(expResult, result);
    }

  
    /**
     * Test of calcular method, of class SimuladorController.
     */
    @Test
    public void testCalcular() throws IOException, FileNotFoundException, ClassNotFoundException {
        System.out.println("calcular");
        SimuladorController instance = new SimuladorController (new Experiencia());
        instance.setLargura(1);
        instance.settExterior(0);
        instance.settInterior(0);
        instance.addMaterialAbordado(new Material());
        double expResult = 0.0;
        double result = instance.calcular();
        assertEquals(expResult, result, 0.0);
    }
    
}
