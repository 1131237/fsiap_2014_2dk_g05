/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package imp;

import model.ListaMateriais;
import model.Material;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;


/**
 *
 * @author Bruno
 */
public class LerConstantesTest {
    
    public LerConstantesTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of importar method, of class LerConstantes.
     */
    @Test
    public void testImportar() {
        System.out.println("importar");
        ListaMateriais lm = new ListaMateriais();
        LerConstantes instance = new LerConstantes();
        instance.importar_pt(lm);
        ListaMateriais aux = new ListaMateriais();
        Material m1 = new Material ("Bolsa de Ar", 0.03);
        Material m2 = new Material ("Madeira (Cortiça)", 0.0045);
        Material m3 = new Material ("Madeira (Pinho)", 0.13);
        Material m4 = new Material ("Tijolo", 0.6);
        Material m5 = new Material ("Esferovite", 0.03);
        Material m6 = new Material ("Borracha", 0.17);
        Material m7 = new Material ("Silicone", 0.04);
        Material m8 = new Material ("Vidro", 0.79);
        Material m9 = new Material ("Fibra de Vidro", 0.05);
        Material m10 = new Material ("Lã Mineral", 0.04);
        Material m11 = new Material ("Celulose", 0.03);
        Material m12 = new Material ("Espuma de Poliuretano", 0.02);
        Material m13 = new Material ("Ferro", 80.2);
        
        aux.addMaterial(m1);
        aux.addMaterial(m2);
        aux.addMaterial(m3);
        aux.addMaterial(m4);
        aux.addMaterial(m5);
        aux.addMaterial(m6);
        aux.addMaterial(m7);
        aux.addMaterial(m8);
        aux.addMaterial(m9);
        aux.addMaterial(m10);
        aux.addMaterial(m11);
        aux.addMaterial(m12);
        aux.addMaterial(m13);
        
        assertEquals(lm.toString(), aux.toString());
        
    }
    
}
