/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Bruno
 */
public class ConversõesTest {
    
    public ConversõesTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of CelsiusToFahrenheit method, of class Conversões.
     */
    @Test
    public void testCelsiusToFahrenheit() {
        System.out.println("CelsiusToFahrenheit");
        double celsius = 20;
        Conversões instance = new Conversões();
        double expResult = 68;
        double result = instance.CelsiusToFahrenheit(celsius);
        assertEquals(expResult, result, 0.0);

    }

    /**
     * Test of FahrenheitToCelsius method, of class Conversões.
     */
    @Test
    public void testFahrenheitToCelsius() {
        System.out.println("FahrenheitToCelsius");
        double fahrenheit = 30.0;
        Conversões instance = new Conversões();
        double expResult = -1.11;
        double result = instance.FahrenheitToCelsius(fahrenheit);
        assertEquals(expResult, result, 0.0001);
    }

    /**
     * Test of CelsiusToKelvin method, of class Conversões.
     */
    @Test
    public void testCelsiusToKelvin() {
        System.out.println("CelsiusToKelvin");
        double celsius = 40.0;
        Conversões instance = new Conversões();
        double expResult = 313.15;
        double result = instance.CelsiusToKelvin(celsius);
        assertEquals(expResult, result, 0.0);

    }

    /**
     * Test of KelvinToCelsius method, of class Conversões.
     */
    @Test
    public void testKelvinToCelsius() {
        System.out.println("KelvinToCelsius");
        double kelvin = 50.0;
        Conversões instance = new Conversões();
        double expResult = -223.15;
        double result = instance.KelvinToCelsius(kelvin);
        assertEquals(expResult, result, 0.0000000001);
        
    }

    /**
     * Test of KelvinToFahrenheit method, of class Conversões.
     */
    @Test
    public void testKelvinToFahrenheit() {
        System.out.println("KelvinToFahrenheit");
        double kelvin = 51.0;
        Conversões instance = new Conversões();
        double expResult = -367.87;
        double result = instance.KelvinToFahrenheit(kelvin);
        assertEquals(expResult, result, 0.01);
       
    }

    /**
     * Test of FahrenheitToKelvin method, of class Conversões.
     */
    @Test
    public void testFahrenheitToKelvin() {
        System.out.println("FahrenheitToKelvin");
        double farhenheit = 50.0;
        Conversões instance = new Conversões();
        double expResult = 283.15;
        double result = instance.FahrenheitToKelvin(farhenheit);
        assertEquals(expResult, result, 0.0000000000001);

    }
    
}
