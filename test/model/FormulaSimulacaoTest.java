/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Daniel
 */
public class FormulaSimulacaoTest {
    
    public FormulaSimulacaoTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of calcularCondutividade method, of class FormulaSimulacao.
     */
    @Test
    public void testCalcularCondutividade() {
        System.out.println("calcularCondutividade");
        int tempInterior = 32;
        int tempExterior = 14;
        double largura = 20;
        List<Material> materiais = new ArrayList<>();
        materiais.add(new Material ("teste", 0.2));
        materiais.add(new Material ("teste2", 0.4));
        FormulaSimulacao instance = new FormulaSimulacao();
        double expResult = 24;
        double result = instance.calcularCondutividade(tempInterior, tempExterior, largura, materiais);
        assertEquals(expResult, result, 0.0);
    }

    /**
     * Test of calcularResistência method, of class FormulaSimulacao.
     */
    @Test
    public void testCalcularResistência() {
        System.out.println("calcularResist\u00eancia");
        int tempInterior = 15;
        int tempExterior = 20;
        double largura = 6.0;
        List<Material> materiais = new ArrayList<>();
        materiais.add(new Material("Esferovite",0.03));
        FormulaSimulacao instance = new FormulaSimulacao();
        double expResult = 2.0;
        double result = instance.calcularResistência(tempInterior, tempExterior, largura, materiais);
        assertEquals(expResult, result, 0.0);

    }
    
}
