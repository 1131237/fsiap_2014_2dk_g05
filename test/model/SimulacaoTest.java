/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Daniel
 */
public class SimulacaoTest {
    
    public SimulacaoTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of addMaterialAbordado method, of class Simulacao.
     */
    @Test
    public void testAddMaterialAbordado() {
        System.out.println("addMaterialAbordado");
        Material m = new Material ();
        Simulacao instance = new Simulacao();
        boolean expResult = true;
        boolean result = instance.addMaterialAbordado(m);
        assertEquals(expResult, result);
    }

    /**
     * Test of calcular method, of class Simulacao.
     */
    @Test
    public void testCalcular() {
        System.out.println("calcular");
        Simulacao instance = new Simulacao();
        instance.setLargura(1);
        instance.settExterior(0);
        instance.settInterior(0);
        List<Material> materiais = new ArrayList<>();
        materiais.add(new Material ());
        instance.setMatUtilizar(materiais);
        double expResult = 0.0;
        double result = instance.calcular();
        assertEquals(expResult, result, 0.0);
    }
    
}
