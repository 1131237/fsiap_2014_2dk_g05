package model;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.util.ArrayList;
import java.util.List;
import model.ListaMateriais;
import model.Material;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author ASUS
 */
public class ListaMateriaisTest {
    
   
    
    

    /**
     * Test of addMaterial method, of class ListaMateriais.
     */
    @Test
    public void testAddMaterial() {
        System.out.println("addMaterial");
        Material material = new Material("Ferro",111);
        ListaMateriais instance = new ListaMateriais();
        instance.addMaterial(material);
        if(!instance.addMaterial(material)){
            assertTrue(true);
        }else{
            assertFalse(true);
        }
        
        Material mat = new Material("teste", 123);
        
        if(instance.addMaterial(mat)){
            assertTrue(true);
        }else{
            assertFalse(true);
        }
        
    }

    /**
     * Test of getListaCondutibilidade method, of class ListaMateriais.
     */
    @Test
    public void testGetListaCondutibilidade() {
        
        System.out.println("getListaCondutibilidade");
        ListaMateriais instance = new ListaMateriais();
        instance.addMaterial(new Material("teste",111));
        List<Double> expResult = instance.getListaCondutibilidade();
        List<Double> result = new ArrayList<>();
        result.add((double)111);
        assertEquals(expResult.get(0), result.get(0));
    }
    
}
